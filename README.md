# CoLa 

> This repository displays the public sets from CoLa. Sets are merged and then splitted according to level of correctness (correct or incorrect).

# Description

The Corpus of Linguistic Acceptability (CoLA) in its full form consists of 10657 sentences from 23 linguistics publications, expertly annotated for acceptability (grammaticality) by their original authors. The public version provided here contains 9594 sentences belonging to training and development sets, and excludes 1063 sentences belonging to a held out test set.

Data has been split into an in-domain set with sentences from 17 sources and an out-of-domain set with the remaining 6 sources. The in-domain set is split into train/dev/test sections, and the out-of-domain set is split into dev/test sections. The test sets are not made public. For convenience, each dataset is provided is provided twice, in raw form and in tokenized form (from the NLTK tokenizer). We are only interested in the raw form. The public data is split into the following files:
- raw/in_domain_train.tsv (8551 lines)
- raw/in_domain_dev.tsv (527 lines)
- raw/out_of_domain_dev.tsv (516 lines)

Each line in the .tsv files consists of 4 tab-separated columns.
- Column 1:	the code representing the source of the sentence.
- Column 2:	the acceptability judgment label (0=unacceptable, 1=acceptable).
- Column 3:	the acceptability judgment as originally notated by the author.
- Column 4:	the sentence.

This decription is extracted from the [GitHub repository of CoLa](https://nyu-mll.github.io/CoLA/).

# Source

The dataset can be downloaded [here](https://nyu-mll.github.io/CoLA/cola_public_1.1.zip), or can be directly found in the directory [source](#source) of the repository.

To merge this file: `cat source/*.tsv > full_set.csv`

This file is located in [source/full_set.csv](#source/full_set.tsv). It consists of 9,594 sentences, labeled as correct (1) or incorrect (0). Additional information are provided in the third column, highlighting past labellisation.

# Sets

We are interested in:
- correct sentences
- incorrect sentences

To split the merged file [source/full_set.csv](#source/full_set.csv) into two files in [sets](#sets): `python3 extract.py`.

The sets can be found in [sets/](#sets)

# Reference

@article{warstadt2018neural,
    title={Neural Network Acceptability Judgments},
    author={Warstadt, Alex and Singh, Amanpreet and Bowman, Samuel R},
    journal={arXiv preprint arXiv:1805.12471},
    year={2018}
}