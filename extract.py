import pandas as pd
import argparse

def save_csv(df, path):
    return df.to_csv(path, index=False)

def main(args):
    try:
        df = pd.read_csv(args.input, sep="\t")
    except:
        print("Full set not found.")
        return 0

    first_row = list(df.columns)
    df.columns = ["Code", "Evaluation", "Pre-evaluation", "Sentence"]
    df = df.append({'Code': first_row[0], 'Evaluation': first_row[1], 'Sentence': first_row[3]}, ignore_index=True)
    df = df.astype({"Evaluation": int})

    df_correct = df[df['Evaluation'] == 1][['Sentence', 'Evaluation']]
    df_incorrect = df[df['Evaluation'] == 0][['Sentence', 'Evaluation']]

    save_csv(df_correct, 'sets/correct.csv')
    save_csv(df_incorrect, 'sets/incorrect.csv')

if __name__ == "__main__":
    # Define and parse program input
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="path of tsv file", default="source/full_set.tsv")
    args = parser.parse_args()

    main(args)